import os
import requests

TOKEN = "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"
HEADERS = {'Authorization': f'Bearer {TOKEN}'}
URL = "https://api.github.com/repos/boto/boto3/pulls"


def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """

    # Write your code here

    def response(info):
        res = requests.get(URL, headers=HEADERS, params=info)
        return res.json()

    def fillArr(i, arr):
        title = data[i]["title"]
        num = data[i]["number"]
        url = data[i]["html_url"]
        obj = {"title": title, "num": num, "link": url}
        arr.append(obj)

    def sort_response(data):
        arr = []
        for i in range(len(data)):
            label = data[i]['labels']
            if label:
                for j in label:
                    if j["name"] == state:
                        fillArr(i, arr)

            if(state == "open" or state == "closed"):
                fillArr(i, arr)

        return arr
        
    if(state == "open"):
        data = response({"state": state, "per_page": 100})
        return sort_response(data)
    elif(state == "closed"):
        data = response({"state": state, "per_page": 100})
        return sort_response(data)
    elif(state == "bug"):
        data = response({"state": "all", "per_page": 100})
        return sort_response(data)
    elif(state == "needs-review"):
        data = response({"state": "all", "per_page": 100})
        return sort_response(data)

