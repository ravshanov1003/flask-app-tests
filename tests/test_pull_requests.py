from handlers.pull_requests import get_pull_requests
from mock import patch
import unittest

# Write your tests here


class TestRequests(unittest.TestCase):
    @patch('requests.get')
    def test_requests(self, value):
        value.return_value.json.return_value = [
            {
                "state": "open",
                "title": "title",
                "number": 1,
                "html_url": "url",
                "labels": [{"name": "bug"}]
            },
            {
                "state": "closed",
                "title": "title",
                "number": 2,
                "html_url": "url",
                "labels": [{"name": "bug"}]
            },
            {
                "state": "open",
                "title": "title",
                "number": 3,
                "html_url": "url",
                "labels": [{"name": "needs-review"}]
            },
            {
                "state": "closed",
                "title": "title",
                "number": 4,
                "html_url": "url",
                "labels": [{"name": "needs-review"}]
            }
        ]
        expected_res = [
            {
                "state": "open",
                "title": "title",
                "num": 1,
                "link": "url",
                "labels": [{"name": "bug"}]
            },
            {
                "state": "closed",
                "title": "title",
                "num": 2,
                "link": "url",
                "labels": [{"name": "bug"}]
            },
            {
                "state": "open",
                "title": "title",
                "num": 3,
                "link": "url",
                "labels": [{"name": "needs-review"}]
            },
            {
                "state": "closed",
                "title": "title",
                "num": 4,
                "link": "url",
                "labels": [{"name": "needs-review"}]
            }
        ]
        res1 = get_pull_requests("open")
        res2 = get_pull_requests("closed")
        res3 = get_pull_requests("bug")
        res4 = get_pull_requests("needs-review")
        self.assertEqual(res1, expected_res[0])
        self.assertEqual(res2, expected_res[1])
        self.assertEqual(res3, expected_res[2])
        self.assertEqual(res4, expected_res[3])


if __name__ == '__main__':
    unittest.main()


def json_get_keys(data):
    count = 0
    test = 0
    for i in range(len(data)):
        if data[i].get('title') and data[i].get('num') and data[i].get('link') is not None:
            test = 1
        count += 1
        if count == 100:
            test = 2
    if test == 1:
        print("Correct keys exist in JSON data")
    else:
        print("Keys doesn't exist in JSON data")
    if test == 2:
        print("There are 100 per pages")
    else:
        print("per_page not enough")


def state_func(state):
    if state == "open":
        return json_get_keys(get_pull_requests(state))
    if state == "closed":
        return json_get_keys(get_pull_requests(state))


state_func("open")
state_func("closed")
